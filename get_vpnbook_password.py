#! /usr/bin/env python2
# By Rupe July 22, 2017
# Fetch vpnbook password
#

from contextlib import closing
from thread import start_new_thread
from time import asctime
from Tkinter import *
from urllib2 import urlopen


class VpnBook(Tk):

  def __init__(self):
    Tk.__init__(self)
    self.resizable(0, 0)
    self.title("VpnBook Password")
    self.protocol("WM_DELETE_WINDOW", self.quit)

    gf = LabelFrame(self, text='Fetch', relief=GROOVE, labelanchor='nw')
    gf.grid(row=1, column=1, padx=4)
    Button(gf,
           text="Get vpnbook password",
           fg="blue",
           command=self.get_passwd).grid(row=1,
                                         column=1,
                                         columnspan=2)
    Button(gf,
           text="Save",
           fg="green",
           command=self.save_password).grid(row=2,
                                            column=1,
                                            columnspan=1,
                                            pady=4)
    Button(gf,
           text="Quit",
           fg="red",
           command=self.quit).grid(row=2,
                                   column=2,
                                   pady=8)
    self.output = Text(self,
                       foreground="green",
                       background="white",
                       highlightbackground="blue",
                       highlightcolor="white",
                       wrap=WORD,
                       height=6,
                       width=25)
    self.output.grid(row=1, column=2, rowspan=1, padx=6, pady=6)

  def write(self, passwd):
    self.output.insert(END, '%s' % (asctime() + '\n'))
    self.output.insert(END, '%s' % ("\nUsername: vpnbook\n"))
    self.output.insert(END, "Password: %s\n" % (passwd) + '\n')
    self.output.see(END)

  def get_vpnbook_password(self):
    with closing(urlopen("https://twitter.com/vpnbook")) as webpage:
      webpage = webpage.read().split()
      html = webpage.index('Password:') + 1
      passwd = webpage[html].split('</p>')
      Password = ''.join(passwd)
    return Password

  def get_passwd(self):
    start_new_thread(self.get_password, ())

  def get_password(self):
    Password = self.get_vpnbook_password()
    self.write(Password)

  def save_password(self):
    start_new_thread(self.save_auth_file, ())

  def save_auth_file(self):
    with open('auth.txt', 'w') as authfile:
      authfile.write("vpnbook \n" + "%s \n" % self.get_vpnbook_password())
      self.write(self.get_vpnbook_password())
    self.output.insert(END, 'Saved to: auth.txt\n')
    self.output.see(END)


if __name__ == '__main__':
  mw = VpnBook()
  mw.mainloop()