# AUTOVPNBOOK #
## Automatically Login And Connect To VPNBook ##
***

** [Screenshot](https://drive.google.com/open?id=0B79r4wTVj-CZNzBfOVpaRnRQMHc) ** :wink:

## Step 1: ##
* Download [autovpnbook repository](https://bitbucket.org/ruped24/autovpnbook/get/aea3449ea374.zip) and unzip.

* Download All [VPNBOOK](https://www.vpnbook.com/freevpn)'s Server OpenVPN Certificate Bundles. **See Step 2**.

## Step 2: ##
* The **get_vpnbook_bundle.sh** script will download, setup, auto-launch avpnb.sh, and AutoVPNBook Menu.

### Usage: ###
```
#!bash

sudo ./get_vpnbook_bundle.sh
```

### Usage: ###

* **avpnb.sh** will launch AutoVPNBook Menu.

```
#!bash

sudo ./avpnb.sh
```



## [**Anonymity Check**](http://proxydb.net/anon) ##

***

#### Disclaimer:

#### These scripts are written for educational purposes only!

#### We have donated to vpnbook.com. :thumbsup:

#### Legal Disclosure: 

#### We are not affiliated with vpnbook.com in any way.  We are not advocating the use of their or any *[free](https://plus.google.com/117604887745850959716/posts/J8tkLw1Fgjv)* VPN service. ####